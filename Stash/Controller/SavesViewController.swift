//
//  SavesViewController.swift
//  Stash
//
//  Created by Florian on 8/20/19.
//  Copyright © 2019 Florian Abel. All rights reserved.
//

import UIKit

class SavesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, dataInteraction {
    func updateView() {
        DispatchQueue.main.async {
            self.tableView.reloadData()
            self.refreshControl.endRefreshing()
        }
    }
    
    

    @IBOutlet weak var myStashsLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    // Table variables
    private var cellHeights: [IndexPath : CGFloat] = [:]
    private let refreshControl = UIRefreshControl()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setDelegates()
        DataSingleton.shared.updateMyStashs()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tableView.reloadData()
    }
    
    // ==========================================================================================
    // Setup
    // ==========================================================================================
    //
   
    func setDelegates () {
        DataSingleton.shared.setDelegate(self)
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
    // ==========================================================================================
    // Tableview methods
    // ==========================================================================================
    //
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DataSingleton.shared.myStashContainer.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let stash = DataSingleton.shared.myStashContainer[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "SavedStashTableViewCell", for: indexPath) as! SavedStashTableViewCell
        cell.selectionStyle = .none
        cell.setSavedStashTableCell(stash: stash)
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cellHeights[indexPath] = cell.frame.size.height
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellHeights[indexPath] ?? 50.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath)
        // self.performSegue(withIdentifier: "feedItemDetailSegue", sender: self)
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
