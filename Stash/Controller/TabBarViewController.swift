//
//  TabBarViewController.swift
//  Stash
//
//  Created by Florian on 8/15/19.
//  Copyright © 2019 Florian Abel. All rights reserved.
//

import UIKit

class TabBarViewController: UITabBarController, UITabBarControllerDelegate {

    var currentStashObjectPreview: StashObjectPreview?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if AuthenticationProvider.shared.askForNps {
            NPSAlert()
        }
    }
    
    func NPSAlert() {
        let storyboard = UIStoryboard(name: "NPS", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "NPSViewController") as! NPSViewController
        controller.providesPresentationContextTransitionStyle = true
        controller.definesPresentationContext = true
        controller.modalPresentationStyle = .custom
        present (controller, animated: true, completion: nil)
    }
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        
        if viewController == tabBarController.viewControllers?[1] {
            findClosestStashObject()
            displayActionSheet()
            return false
        } else {
            return true
        }
    }
    
    
    func displayActionSheet() {
        var message = "There seems to be no objects around you..."
        if let locationName = currentStashObjectPreview?.title {
            message = "Set stash at: " + locationName
        }
        
        let alert = UIAlertController(title: "Share your love....", message: message, preferredStyle: .actionSheet)
        if currentStashObjectPreview != nil {
            //alert.addAction(UIAlertAction(title: "I am somewhere else", style: .default, handler: nil))
            alert.addAction(UIAlertAction(title: "Set stash", style: .default, handler: self.setStash))
            alert.addAction(UIAlertAction(title: "Set stash with content", style: .default, handler:self.displaySetStashViewController))
        } else {
            alert.addAction(UIAlertAction(title: "Create a new Object", style: .default, handler: self.displayCreateStashObjectViewController))
            //alert.addAction(UIAlertAction(title: "Set stash without object", style: .default, handler: nil))
            //alert.addAction(UIAlertAction(title: "//Add content without object", style: .default, handler:self.displaySetStashViewController))
        }
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true)
    }
    
    func findClosestStashObject() {
        let stashObjectPreviews = DataSingleton.shared.stashObjectPreviewContainer.filter({return $0.distance <= 250.0}).sorted(by: {$0.distance < $1.distance})
        if stashObjectPreviews.count > 0  {
            currentStashObjectPreview = stashObjectPreviews[0]
        }
    }
    
    func setStash(alert: UIAlertAction!) {
        if let currentStashObjectPreview = currentStashObjectPreview, let currentLocation = LocationProvider.shared.currentLocation {
            print(currentStashObjectPreview.stashObjectId)
            //let newStashOld = StashToJSON(stashObjectId: currentStashObjectPreview.stashObjectId, location: currentLocation)
            let newStash = Stash(stashObjectId: currentStashObjectPreview.stashObjectId, location: currentLocation)
            DataSingleton.shared.addNew(stash: newStash, completion: ({ result in
                switch result {
                case .success(_):
                    self.stashSetAlert(true);
                case .failure(let error):
                    print("Could not post stash Object")
                    print(error)
                    self.stashSetAlert(false);
                }
            }))

//            DataSingleton.shared.postStash(stashToSave: newStash, completion: ({ result in
//                switch result {
//                case .success(let stashObjectId):
//                    self.stashSetAlert(true);
//                case .failure(let error):
//                    print("Could not post stash Object")
//                    print(error)
//                    self.stashSetAlert(false);
//                }
//            }))

        }
    }
    
    func displaySetStashViewController(alert: UIAlertAction!) {
        let storyboard = UIStoryboard(name: "SetStash", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "SetStashViewController") as! SetStashViewController
        if let currentStashObjectPreview = currentStashObjectPreview {
            vc.currentStashObjectPreview = currentStashObjectPreview
        }
        present(vc, animated: true, completion: nil)
    }
    
    func displayCreateStashObjectViewController(alert: UIAlertAction!) {
        let storyboard = UIStoryboard(name: "CreateStashObject", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "createStashObject") as! UINavigationController
        present(vc, animated: true, completion: nil)
    }
    
    func stashSetAlert(_ success: Bool) {
        if success {
            let alert = UIAlertController(title: "Stash has been set", message: "Thank you for being part of the community :-)", preferredStyle: .alert)
            //alert.addAction(UIAlertAction(title: "Share", style: .default, handler: self.setStash))
            alert.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: nil))
            DispatchQueue.main.async {
                self.present(alert, animated: true)
            }
        } else {
            let alert = UIAlertController(title: "Stash could not be set", message: "We apologize, but something went wrong :-( Please try again", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            alert.addAction(UIAlertAction(title: "Try again", style: .default, handler: self.setStash))
            DispatchQueue.main.async {
                self.present(alert, animated: true)
            }
        }
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
