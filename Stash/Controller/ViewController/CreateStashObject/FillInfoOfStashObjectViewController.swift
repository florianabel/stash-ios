//
//  FillInfoOfStashObjectViewController.swift
//  Stash
//
//  Created by Florian on 8/9/19.
//  Copyright © 2019 Florian Abel. All rights reserved.
//

import Foundation
import UIKit

class FillInfoOfStashObjectViewController: UIViewController, UITextFieldDelegate, UITextViewDelegate, stashPictureInteraction {
    func setStashPicture(image: UIImage) {
        stashObjectImageView.image = image
    }
    

    @IBAction func addSubtitleAction(_ sender: Any) {
        setSubtitleStackView()
        addSubtitleButton.isHidden = true
        
    }
    @IBAction func addTakePictureAction(_ sender: Any) {
        subtitleField.resignFirstResponder()
        setTakePictureStackView()
        status = 2
        addPictureButton.isHidden = true
    }
    @IBAction func removePicture(_ sender: Any) {
        pictureView.image = nil
        stylePictureView()
    }
    @IBAction func addDesriptionAction(_ sender: Any) {
        setDescriptionStackView()
        addDescriptionButton.isHidden = true
    }
    
    
    @IBOutlet weak var navigationBar: UINavigationItem!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var titleStackView: UIStackView!
    @IBOutlet weak var titleField: UITextField!
    @IBOutlet weak var addSubtitleButton: UIButton!
    @IBOutlet weak var subtitleStackView: UIStackView!
    @IBOutlet weak var subtitleField: UITextField!
    @IBOutlet weak var addPictureButton: UIButton!
    @IBOutlet weak var takePictureStackView: UIStackView!
    @IBOutlet weak var pictureView: UIImageView!
    @IBOutlet weak var takePictureButton: UIButton!
    @IBOutlet weak var editPictureButton: UIButton!
    @IBOutlet weak var removePictureButton: UIButton!
    @IBOutlet weak var addDescriptionButton: UIButton!
    @IBOutlet weak var stashObjectImageView: UIImageView!
    @IBOutlet weak var descriptionStackView: UIStackView!
    @IBOutlet weak var descriptionField: UITextView!
    @IBOutlet weak var keyboardView: UIView!
    @IBOutlet weak var keyboardViewHeight: NSLayoutConstraint!
    @IBOutlet weak var continueButton: UIButton!
    
    
    var SOType: String?
    var statusOptions = ["title", "subtitle", "takePicture", "description"]
    var status = 0
    var activeTextField: UITextField?
    var activeTextView: UITextView?
    var keyboardHeight: CGFloat?
    
    //TapGestureRecognizer
    var singleTapGestureRecognizer: UITapGestureRecognizer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupSingleTapRecognizer()
        setupKeyboard()
        descriptionField.layer.cornerRadius = 5
        descriptionField.layer.borderColor = UIColor.gray.withAlphaComponent(0.5).cgColor
        descriptionField.layer.borderWidth = 0.5
        descriptionField.clipsToBounds = true
        continueButton.layer.borderWidth = 1.0
        continueButton.layer.cornerRadius = 5
        continueButton.layer.borderColor = UIColor.blue.cgColor
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        adjustView()
        stylePictureView()
        
    }
    
    func stylePictureView() {
        if pictureView.image != nil {
            takePictureButton.isHidden = true
            editPictureButton.isHidden = false
            removePictureButton.isHidden = false
        } else {
            takePictureButton.isHidden = false
            editPictureButton.isHidden = true
            removePictureButton.isHidden = true
        }
    }
    func setupSingleTapRecognizer () {
        let singleTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(singleTap))
        singleTapGestureRecognizer.numberOfTapsRequired = 1
        singleTapGestureRecognizer.isEnabled = true
        singleTapGestureRecognizer.cancelsTouchesInView = false
        self.singleTapGestureRecognizer = singleTapGestureRecognizer
    }
    
    func setupKeyboard() {
        // listen to keyboard show event
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        // listen to keyboard hide event
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func singleTap(sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }

    
    // keyboard shown
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            keyboardHeight = keyboardRectangle.height
        }
        if let singleTapGestureRecognizer = singleTapGestureRecognizer {
            scrollView.addGestureRecognizer(singleTapGestureRecognizer)
        }
        keyboardViewHeight.constant = 260.0
        self.view.layoutIfNeeded()
        if let activeTextField = activeTextField {
            scrollView.scrollRectToVisible(activeTextField.frame, animated: true)
        } else if activeTextView != nil {
            scrollView.scrollRectToVisible(keyboardView.frame, animated: true)
        }
    }
    
    // keyboard hidden
    @objc func keyboardWillHide(notification: NSNotification) {
        if let singleTapGestureRecognizer = singleTapGestureRecognizer {
             scrollView.removeGestureRecognizer(singleTapGestureRecognizer)
        }
        keyboardViewHeight.constant = 0.0
        self.view.layoutIfNeeded()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.activeTextField = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.activeTextField = nil
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        self.activeTextView = textView
        return true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        self.activeTextView = nil
    }
    
    func setTitleStackView () {
        titleStackView.isHidden = false
        titleField.becomeFirstResponder()
        self.scrollView.scrollRectToVisible(self.titleStackView.frame, animated: true)
    }
    
    func setSubtitleStackView () {
        subtitleStackView.isHidden = false
        subtitleField.becomeFirstResponder()
        self.scrollView.scrollRectToVisible(self.subtitleStackView.frame, animated: true)
    }
    
    func setTakePictureStackView () {
        takePictureStackView.isHidden = false
        self.scrollView.scrollRectToVisible(self.takePictureStackView.frame, animated: true)
    }
    
    func setDescriptionStackView () {
        descriptionStackView.isHidden = false
        descriptionField.becomeFirstResponder()
        scrollView.scrollRectToVisible(keyboardView.frame, animated: true)
    }
    
    func moveElementAboveKeyboard (element: CGRect) {
        
        if let keyboardHeight = keyboardHeight {
            //let oldBottomConstraint = bottomConstraint.constant
            //bottomConstraint.constant = keyboardHeight + oldBottomConstraint
            let scrollRect = CGRect(x: element.origin.x, y: element.origin.y + keyboardHeight, width: element.width, height: keyboardHeight * 2)
            print(scrollRect)
            print(descriptionStackView.frame)
            self.scrollView.scrollRectToVisible(scrollRect, animated: true)
            self.scrollView.setContentOffset(element.origin, animated: true)
            print("scrolled")
        }
    }
    
    func adjustView() {
        
        switch statusOptions[status] {
        case "title":
            setTitleStackView()
            status = 0
        case "subtitle":
            setSubtitleStackView()
            status = 1
        case "takePicture":
            setTakePictureStackView()
            status = 2
        case "description":
            setDescriptionStackView()
            status = 3
        default:
            print("error in switch")
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "TakePictureForStashObjectSegue" {
            if let takePictureVC: TakePictureForStashObjectViewController = segue.destination as? TakePictureForStashObjectViewController{
                takePictureVC.delegate = self
            }
        } else if segue.identifier == "SummarizeStashObjectSegue" {
            print("Segue1")
            if let summarizeStashObjectVC: SummarizeStashObjectViewController = segue.destination as? SummarizeStashObjectViewController, let SOType = SOType, let SOTitle = titleField.text, let SOSubtitle = subtitleField.text, let SODescription = descriptionField.text, let SOPicture = pictureView.image {
                summarizeStashObjectVC.SOType = SOType
                summarizeStashObjectVC.SOTitle = SOTitle
                summarizeStashObjectVC.SOSubtitle = SOSubtitle
                summarizeStashObjectVC.SODescription = SODescription
                summarizeStashObjectVC.SOImage = SOPicture
            }
        }
    }
}
