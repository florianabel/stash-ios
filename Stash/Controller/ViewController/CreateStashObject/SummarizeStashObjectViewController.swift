//
//  SummarizeStashObjectViewController.swift
//  Stash
//
//  Created by Florian on 8/12/19.
//  Copyright © 2019 Florian Abel. All rights reserved.
//

import UIKit

class SummarizeStashObjectViewController: UIViewController {


    @IBAction func postStashObjectAction(_ sender: Any) {
        postStashObject()
    }
    
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var uploadButton: UIButton!
    
    var SOId: String?
    var SOType: String?
    var SOTitle: String?
    var SOSubtitle: String?
    var SODescription: String?
    var SOImage: UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        typeLabel.text = SOType
        titleLabel.text = SOTitle
        subtitleLabel.text = SOSubtitle
        descriptionLabel.text = SODescription
        imageView.image = SOImage
        uploadButton.layer.borderWidth = 1.0
        uploadButton.layer.cornerRadius = 5
        uploadButton.layer.borderColor = UIColor.blue.cgColor
        // Do any additional setup after loading the view.
    }
    
    
    func postStashObject () {
        if let SOType = SOType, let SOTitle = SOTitle, let SOSubtitle = SOSubtitle, let SODescription = SODescription, let SOImage = SOImage, let SOLocation = LocationProvider.shared.currentLocation {
            //let stashObject = StashObjectToJSON(type: SOType, title: SOTitle, subtitle: SOSubtitle, description: SODescription, location: SOLocation, image: SOImage)
            let newImage = Image(image: SOImage, location: SOLocation)
            let newStashObject = StashObject(type: SOType, title: SOTitle, subtitle: SOSubtitle, description: SODescription, location: SOLocation, images: [newImage])
            DataSingleton.shared.addNew(stashObject: newStashObject, completion: ({ result in
                switch result {
                case .success(_):
                    DispatchQueue.main.async {
                        self.performSegue(withIdentifier: "StashObjectSuccessfullyPostedSegue", sender: nil)
                    }
                case .failure(let error):
                    print("Could not post stash Object")
                    print(error)
                }
            }))
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "StashObjectSuccessfullyPostedSegue" {
            if let postedStashObjectVC: PostStashObjectViewController = segue.destination as? PostStashObjectViewController, let SOId = SOId, let SOType = SOType, let SOTitle = SOTitle, let SOSubtitle = SOSubtitle, let SODescription = SODescription, let SOImage = SOImage {
                postedStashObjectVC.SOId = SOId
                postedStashObjectVC.SOType = SOType
                postedStashObjectVC.SOTitle = SOTitle
                postedStashObjectVC.SOSubtitle = SOSubtitle
                postedStashObjectVC.SODescription = SODescription
                postedStashObjectVC.SOImage = SOImage
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
