//
//  FeedItemDetailViewController.swift
//  Stash
//
//  Created by Florian on 7/17/19.
//  Copyright © 2019 Florian Abel. All rights reserved.
//

import UIKit

class DiscoverItemDetailViewController: UIViewController {
    
    // ==========================================================================================
    // Variables and constants
    // ==========================================================================================
    //
    var feedItem: FeedItem?
    
    
    
    // ==========================================================================================
    // IBOutlets
    // ==========================================================================================
    //

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    
    
    // ==========================================================================================
    // IBActions
    // ==========================================================================================
    @IBAction func closeDetailView(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    //
    
    
    
    // ==========================================================================================
    // Lifecycle hooks
    // ==========================================================================================
    //
    override func viewDidLoad() {
        super.viewDidLoad()
        styleView()
        populateView()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        populateView()
    }
    
    
    func styleView() {
        descriptionLabel.layer.cornerRadius = 5
        descriptionLabel.layer.borderColor = UIColor.gray.withAlphaComponent(0.5).cgColor
        descriptionLabel.layer.borderWidth = 0.5
        descriptionLabel.clipsToBounds = true
    }
    // ==========================================================================================
    // Populate View
    // ==========================================================================================
    //
    func populateView() {
        if let detailItem = feedItem {
            titleLabel.text = detailItem.title
            subtitleLabel.text = detailItem.subtitle
            if detailItem.images.count != 0 {
                guard let image = detailItem.images[0].image else { return }
                imageView.image = image
            }
            descriptionLabel.text = detailItem.description
            categoryLabel.text = detailItem.category
            distanceLabel.text = String(detailItem.distance)
        }
    }
    
}
