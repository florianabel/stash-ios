//
//  LoginViewController.swift
//  Stash
//
//  Created by Florian on 8/3/19.
//  Copyright © 2019 Florian Abel. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var welcomeLabel: UILabel!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        var userName = UIDevice.current.name
        let searchString = "’s iPhone"
        if userName.hasSuffix(searchString) {
            userName = String(userName.split(separator: "’")[0])
            welcomeLabel.text = "Welcome " + userName + "!"
        }
        emailField.delegate = self
        passwordField.delegate = self
        // Do any additional setup after loading the view.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        emailField.resignFirstResponder()
        passwordField.resignFirstResponder()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    @IBAction func loginAction(_ sender: Any) {
        if let email = emailField.text, let password = passwordField.text {
            //let user = UserToAuthenticate(email: email, password: password)
            let authenticationRequest = AuthenticationRequest(email: email, password: password)
            AuthenticationProvider.shared.loginUser(with: authenticationRequest)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        // get a reference to the second view controller
        //let startupViewController = segue.destination as! StartupViewController
        
        // set a variable in the second view controller with the String to pass
        //startupViewController.email = emailField.text!
        //startupViewController.password = passwordField.text!
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
