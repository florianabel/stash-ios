//
//  ProfileViewController.swift
//  Stash
//
//  Created by Florian on 8/3/19.
//  Copyright © 2019 Florian Abel. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {

    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var metric1Label: UILabel!
    @IBOutlet weak var metric2Label: UILabel!
    @IBOutlet weak var metric3Label: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var userDescriptionLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeUserProfile()
        // Do any additional setup after loading the view.
    }
    

    func initializeUserProfile() {
        guard let user = AuthenticationProvider.shared.user else { return }
        if let profileImage = user.profileImage {
            profileImageView.image = profileImage
        }
        if let username = user.username {
            usernameLabel.text = username
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
