//
//  AddTextAndImageToStashViewController.swift
//  Stash
//
//  Created by Florian on 8/2/19.
//  Copyright © 2019 Florian Abel. All rights reserved.
//

import Foundation
import UIKit
import UserNotifications

class AddTextAndImageToStashViewController: UIViewController, UITextViewDelegate, UNUserNotificationCenterDelegate, stashPictureInteraction {

    func setStashPicture(image: UIImage) {
        stashImageView.image = image
    }
    
    @IBOutlet weak var stashObjectTitle: UILabel!
    @IBOutlet weak var stashTextView: UITextView!
    @IBOutlet weak var stashImageView: UIImageView!
    @IBOutlet weak var takePictureButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        styleView()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
        super.touchesBegan(touches, with: event)
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Why do you recommend this place? What makes it special to you? Use #tags to make it easily discoverable....  Spread the stash.love <3"
            textView.textColor = UIColor.lightGray
        }
    }
    
    func styleView () {
        if stashImageView.image != nil {
            takePictureButton.isHidden = true
        }
        stashTextView.text = "Why do you recommend this place? What makes it special to you? Use #tags to make it easily discoverable....  Spread the stash.love <3"
        stashTextView.textColor = UIColor.lightGray
        stashTextView.layer.cornerRadius = 5
        stashTextView.layer.borderColor = UIColor.gray.withAlphaComponent(0.5).cgColor
        stashTextView.layer.borderWidth = 0.5
        stashTextView.clipsToBounds = true
    }
    
    func postStash () {
//        guard let title = nameField.text, let tags = stashTags.text, let location = LocationSingleton.shared.currentLocation else { return}
//        let stashItem =  UserStashToJSON(tags: tags, location: location, image: stashImageView.image)
//        DataSingleton.shared.postUserStashToServer(stashItem)
        userNotification()
        //        let stashItem =  StashToJSON(title: title, tags: tags, location: location, image: stashImage.image)
        //        DataSingleton.shared.postStashToServer(<#T##stashToSave: StashToJSON##StashToJSON#>, completion: <#T##(Result<String, APIError>) -> Void#>)
    }
    
    func userNotification () {
        //creating the notification content
        let content = UNMutableNotificationContent()
        
        //adding title, subtitle, body and badge
        content.title = "Hey this is Simplified iOS"
        content.subtitle = "iOS Development is fun"
        content.body = "We are learning about iOS Local Notification"
        content.badge = 1
        
        //getting the notification trigger
        //it will be called after 5 seconds
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
        
        //getting the notification request
        let request = UNNotificationRequest(identifier: "SimplifiedIOSNotification", content: content, trigger: trigger)
        
        UNUserNotificationCenter.current().delegate = self
        
        //adding the notification to notification center
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        //displaying the ios local notification when app is in foreground
        completionHandler([.alert, .badge, .sound])
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "TakePictureForStashSegue" {
//            if let takePictureVC: TakePictureViewController = segue.destination as? TakePictureViewController{
//                takePictureVC.delegate = self
//            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
