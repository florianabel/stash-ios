//
//  TakePictureViewController.swift
//  Stash
//
//  Created by Florian on 7/29/19.
//  Copyright © 2019 Florian Abel. All rights reserved.
//

import UIKit
import Foundation
import AVFoundation

class TakePictureViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate, AVCaptureVideoDataOutputSampleBufferDelegate {
    
    var delegate: SetStashViewController?
    
    // Variables for the camera
    let captureSession = AVCaptureSession()
    var frontCamera: AVCaptureDevice?
    var backCamera: AVCaptureDevice?
    var currentCamera: AVCaptureDevice?
    var photoOutput: AVCapturePhotoOutput?
    var cameraPreviewLayer: AVCaptureVideoPreviewLayer?
    
    var takePicture = false
    var takenPhoto: UIImage?
   
    
    @IBOutlet var interfaceView: UIView!
    @IBOutlet weak var takePhotoButton: UIButton!
    
    @IBAction func takePicture(_ sender: Any) {
        takePicture = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCamera()
        self.view.bringSubviewToFront(takePhotoButton)
        // Do any additional setup after loading the view.
    }
    
    func setupCamera () {
        setupCaptureSession()
        setupDevice()
        setupDevice()
        setupInputOutput()
        setupPreviewLayer()
        setupOutput()
        startRunningCaptureSession()
    }
    
    func setupCaptureSession () {
        captureSession.sessionPreset = AVCaptureSession.Preset.photo
    }
    
    func setupDevice () {
        let deviceDiscoverySession = AVCaptureDevice.DiscoverySession(deviceTypes: [AVCaptureDevice.DeviceType.builtInWideAngleCamera], mediaType: .video, position: AVCaptureDevice.Position.unspecified)
        let devices = deviceDiscoverySession.devices
        for device in devices {
            if device.position == AVCaptureDevice.Position.back {
                backCamera = device
            } else if device.position == AVCaptureDevice.Position.front{
                frontCamera = device
            }
        }
        
        currentCamera = backCamera
    }
    
    func setupInputOutput () {
        do {
            let captureDeviceInput = try AVCaptureDeviceInput(device: currentCamera!)
            captureSession.addInput(captureDeviceInput)
            photoOutput?.setPreparedPhotoSettingsArray([AVCapturePhotoSettings(format: [AVVideoCodecKey: AVVideoCodecType.jpeg])], completionHandler: nil)
        } catch {
            print(error.localizedDescription)
        }
    }
    
    func setupPreviewLayer () {
        cameraPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        cameraPreviewLayer?.videoGravity = AVLayerVideoGravity.resize
        cameraPreviewLayer?.connection?.videoOrientation = AVCaptureVideoOrientation.portrait
        cameraPreviewLayer?.frame = self.view.frame
        self.view.layer.addSublayer(cameraPreviewLayer!)
        
        
    }
    
    func setupOutput () {
        let dataOutput = AVCaptureVideoDataOutput()
        dataOutput.videoSettings = [(kCVPixelBufferPixelFormatTypeKey as NSString): NSNumber(value: kCVPixelFormatType_32BGRA)] as [String : Any]
        dataOutput.alwaysDiscardsLateVideoFrames = true
        
        if captureSession.canAddOutput(dataOutput) {
            captureSession.addOutput(dataOutput)
        }
        captureSession.commitConfiguration()
        let queue = DispatchQueue(label: "cameraPreview")
        dataOutput.setSampleBufferDelegate(self, queue: queue)
    }
    
    func startRunningCaptureSession () {
        captureSession.startRunning()
    }
    
    func stopRunningCaptureSession () {
        captureSession.stopRunning()
    }
    
    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        if takePicture {
            takePicture = false
            if let image = self.getImageFromSampleBuffer(buffer: sampleBuffer) {
                takenPhoto = image
                self.delegate?.setStashPicture(image: image)
                DispatchQueue.main.async {
                    self.dismiss(animated: true, completion: nil)
                }
            }
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "CreateStashSegue" {
//            if let createStashVC: ViewController = segue.destination as? TakePictureForStashObjectViewController{
//                createStashVC.takenPhoto = takenPhoto
//            }
        }
    }
    
    func getImageFromSampleBuffer (buffer: CMSampleBuffer) -> UIImage? {
        if let pixelBuffer = CMSampleBufferGetImageBuffer(buffer) {
            let ciImage = CIImage(cvPixelBuffer: pixelBuffer)
            let context = CIContext()
            
            let imageRect = CGRect(x: 0, y: 0, width: CVPixelBufferGetWidth(pixelBuffer), height: CVPixelBufferGetHeight(pixelBuffer))
            
            if let image = context.createCGImage(ciImage, from: imageRect) {
                return UIImage(cgImage: image, scale: UIScreen.main.scale, orientation: .right)
            }
            
        }
        return nil
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}


