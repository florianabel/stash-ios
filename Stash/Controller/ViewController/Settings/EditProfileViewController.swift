//
//  EditProfileViewController.swift
//  Stash
//
//  Created by Florian on 8/7/19.
//  Copyright © 2019 Florian Abel. All rights reserved.
//

import UIKit

class EditProfileViewController: UIViewController {

    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeUserProfile()
        // Do any additional setup after loading the view.
    }
    
    func initializeUserProfile() {
        guard let user = AuthenticationProvider.shared.user else { return }
//        if let profileImage = user.profileImage {
//            profileImageView.image = profileImage
//        }
        emailLabel.text = user.email
        if let username = user.username {
            usernameLabel.text = username
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
