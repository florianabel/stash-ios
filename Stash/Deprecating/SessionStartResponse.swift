//
//  sessionStartResponse.swift
//  Stash
//
//  Created by Florian on 8/2/19.
//  Copyright © 2019 Florian Abel. All rights reserved.
//

import Foundation

struct SessionStartResponse: Decodable {
    var sessionId: Int
}
