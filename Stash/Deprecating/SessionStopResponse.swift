//
//  SessionStopResponse.swift
//  Stash
//
//  Created by Florian on 8/5/19.
//  Copyright © 2019 Florian Abel. All rights reserved.
//

import Foundation

struct SessionStopResponse: Decodable {
    var sessionId: Int
    var status: Bool
}
