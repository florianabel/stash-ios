//
//  ConnectionProvider.swift
//  Stash
//
//  Created by Florian on 8/5/19.
//  Copyright © 2019 Florian Abel. All rights reserved.
//

import Foundation
import UIKit

class AuthenticationProvider {
    
    // ==========================================================================================
    // Variables and Constants
    // ==========================================================================================
    //
    // Singleton instantiation
    static let shared = AuthenticationProvider()
    
    // Variables
    private(set) var user: User?
    private var session: Session?
    private(set) var askForNps: Bool = false
    private(set) var npsRating: NPSRating?
    
    
    // ==========================================================================================
    // Initialization
    // ==========================================================================================
    //
    private init() {
        increaseAppOpenedCounter()
    }
    
    
    // ==========================================================================================
    // Entrance points
    // ==========================================================================================
    //
    
    func checkAuthorization() {
        if fetchUserFromUserDefaults() {
            authenticateUser()
        } else {
            setLoginVC()
        }
    }
    
    // ==========================================================================================
    // Authorization
    // ==========================================================================================
    //
    
    func registerUser(with authenticationRequest: AuthenticationRequest) {
        ConnectionProvider.shared.registerUser(with: authenticationRequest, completion: ({ result in
            switch result {
            case .success(let user):
                self.setUser(user: user)
                self.resetAppOpenedCounter()
                self.resetAskedForNps()
                self.startSession()
                DataSingleton.shared.updateData()
                self.setMainVC()
                print("User successfully registered")
            case .failure(let error):
                print("Could not register user due to: ")
                print(error)
            }
        }))
    }
    
    func loginUser(with authenticationRequest: AuthenticationRequest) {
        ConnectionProvider.shared.loginUser(with: authenticationRequest, completion: ({ result in
            switch result {
            case .success(let user):
                self.setUser(user: user)
                self.resetAppOpenedCounter()
                self.resetAskedForNps()
                self.startSession()
                DataSingleton.shared.updateData()
                self.setMainVC()
                print("User successfully logged in")
            case .failure(let error):
                print("Could not login user due to: ")
                print(error)
            }
        }))
    }
    
    func authenticateUser () {
        if let currentUser = user  {
            ConnectionProvider.shared.authenticate(user: currentUser, completion: ({ result in
                switch result {
                case .success(let user):
                    self.setUser(user: user)
                    self.startSession()
                    print("User successfully authenticated")
                case .failure(let error):
                    print("Could not authenticate user due to: ")
                    print(error)
                    if error != APIError.responseProblem {
                        self.setLoginVC()
                    }
                }
            }))
        } else {
            self.setLoginVC()
        }
    }
    
    func logoutUser() {
        stopSession()
        deleUserFromUserDefaults()
        DataSingleton.shared.emptyContainer()
        setLoginVC()
        print("User successfully logged out")
    }
    
    // ==========================================================================================
    // Storage
    // ==========================================================================================
    //

    func setUser(user: User) {
        self.user = user
        saveUserToUserDefaults()
    }
    
    func saveUserToUserDefaults() {
        guard let user = self.user else { return }
        if let encodedUser = try? JSONEncoder().encode(UserToJSON(user: user)) {
            UserDefaults.standard.set(encodedUser, forKey: "User")
        }
    }
    
    func fetchUserFromUserDefaults () -> Bool {
        if let fetchedUserRaw = UserDefaults.standard.data(forKey: "User"),
            let fetchedUserJSON = try? JSONDecoder().decode(UserFromJSON.self, from: fetchedUserRaw) {
            let fetchedUser = User(userId: fetchedUserJSON.userId, email: fetchedUserJSON.email, token: fetchedUserJSON.token)
            if let profileImage = fetchedUserJSON.profileImage {
                fetchedUser.setProfileImage(imageBase64: profileImage)
            }
            if let username = fetchedUserJSON.username {
                fetchedUser.setUsername(username: username)
            }
            self.user = fetchedUser
            return true
        }
        return false
    }
    
    func deleUserFromUserDefaults () {
        UserDefaults.standard.removeObject(forKey: "User")
    }
    
    // ==========================================================================================
    // Statistics
    // ==========================================================================================
    //
    
    func increaseAppOpenedCounter() {
        var appOpened = UserDefaults.standard.integer(forKey: "AppOpenedCount")
        var askedForNps = UserDefaults.standard.integer(forKey: "AskedForNPS")
        if askedForNps == 0 {
            askedForNps += 1
        }
        print(askedForNps)
        appOpened+=1
        print("Stash has been opened: " + String(appOpened) + " times.")
        
        if appOpened % (4 * askedForNps) == 0 {
            askForNps = true
        }
        
        UserDefaults.standard.set(appOpened, forKey: "AppOpenedCount")
    }
    
    func resetAppOpenedCounter() {
        UserDefaults.standard.set(1, forKey: "AppOpenedCount")
    }
    
    func askedForNps(){
        var askedForNps = UserDefaults.standard.integer(forKey: "AskedForNPS")
        askedForNps+=1
        UserDefaults.standard.set(askedForNps, forKey: "AskedForNPS")
        askForNps = false
    }
    
    func resetAskedForNps() {
        UserDefaults.standard.set(0, forKey: "AskedForNPS")
    }
    
    func sendNPSRating(npsRating: Int) {
        guard let user = self.user else { return }
        let npsRating = NPSRating(npsRating: npsRating)
        ConnectionProvider.shared.submitNPSRating(npsRating: npsRating, from: user, completion: ({ result in
            switch result {
            case .success(let npsRating):
                self.npsRating = npsRating
                print("NPS Rating successfully set")
            case .failure(let error):
                print("Could not set nps rating due to: ")
                print(error)
            }
        }))
    }
    
    // ==========================================================================================
    // Sessions
    // ==========================================================================================
    //
    
    func startSession () {
        guard let user = self.user else { return }
        ConnectionProvider.shared.openSession(for: user, completion: ({ result in
            switch result {
            case .success(let newSession):
                self.session = newSession
                print("Session successfully started")
            case .failure(let error):
                print("Could not start session due to: ")
                print(error)
            }
        }))
    }
    
    func stopSession () {
        guard let session = self.session, let user = self.user else { return }
        ConnectionProvider.shared.closeSession(for: session, from: user, completion: ({ result in
            switch result {
            case .success(let oldSession):
                if self.session?.sessionId == oldSession.sessionId {
                    self.session = nil
                    print("Session successfully closed")
                }
            case .failure(let error):
                print("Could not close session due to: ")
                print(error)
            }
        }))
    }
    
    
    // ==========================================================================================
    // Interface
    // ==========================================================================================
    //
    
    func setLoginVC() {
        DispatchQueue.main.async {
            let storyboard = UIStoryboard.init(name: "Login", bundle: nil)
            let loginVC = storyboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginViewController
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window?.rootViewController = loginVC
        }
    }
    
    func setMainVC() {
        DispatchQueue.main.async {
            let mainStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
            let mainVC = mainStoryboard.instantiateViewController(withIdentifier: "mainVC") as! UITabBarController
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window?.rootViewController = mainVC
        }
    }

}
