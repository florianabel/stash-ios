//
//  ConnectionProvider.swift
//  Stash
//
//  Created by Florian on 8/13/19.
//  Copyright © 2019 Florian Abel. All rights reserved.
//

import Foundation
import CoreLocation

class ConnectionProvider {
    static let shared = ConnectionProvider()
    
    // ==========================================================================================
    // Endpoints
    // ==========================================================================================
    //
    // Production: http://ec2-3-120-32-144.eu-central-1.compute.amazonaws.com
    // Development: http://ec2-18-195-177-18.eu-central-1.compute.amazonaws.com
    private let registerEndpoint: URL = URL(string: "http://ec2-3-120-32-144.eu-central-1.compute.amazonaws.com/register")!
    private let loginEndpoint: URL = URL(string: "http://ec2-3-120-32-144.eu-central-1.compute.amazonaws.com/login")!
    private let authenticationEndpoint: URL = URL(string: "http://ec2-3-120-32-144.eu-central-1.compute.amazonaws.com/authenticate")!
    private let sessionStartEndpoint: URL = URL(string: "http://ec2-3-120-32-144.eu-central-1.compute.amazonaws.com/sessionStart")!
    private let sessionStopEndpoint: URL = URL(string: "http://ec2-3-120-32-144.eu-central-1.compute.amazonaws.com/sessionStop")!
    private let npsRatingEndpoint: URL = URL(string: "http://ec2-3-120-32-144.eu-central-1.compute.amazonaws.com/npsRatingSubmit")!
    private let postStashEndpoint: URL = URL(string: "http://ec2-3-120-32-144.eu-central-1.compute.amazonaws.com/addStash")!
    private let postStashObjectEndpoint: URL = URL(string: "http://ec2-3-120-32-144.eu-central-1.compute.amazonaws.com/addStashObject")!
    private let getStashObjectPreviewsEndpoint: URL = URL(string: "http://ec2-3-120-32-144.eu-central-1.compute.amazonaws.com/getStashObjectPreviews")!
    private let getStashObjectsEndpoint: URL = URL(string: "http://ec2-3-120-32-144.eu-central-1.compute.amazonaws.com/getStashObjects")!
    private let getStashsByIdEndpoint: URL = URL(string: "http://ec2-3-120-32-144.eu-central-1.compute.amazonaws.com/getStashs")!
    private let getStashsByUserEndpoint: URL = URL(string: "http://ec2-3-120-32-144.eu-central-1.compute.amazonaws.com/getMyStashs")!
    private let getImagesEndpoint: URL = URL(string: "http://ec2-3-120-32-144.eu-central-1.compute.amazonaws.com/getImages")!
    
    private init() {
        
    }
    
    // ==========================================================================================
    // Authentication
    // ==========================================================================================
    //
    
    internal func registerUser(with authenticationRequest: AuthenticationRequest,
                       completion: @escaping(Result<User, APIError>)
        -> Void) {
        var urlRequest = URLRequest(url: registerEndpoint)
        urlRequest.httpMethod = "POST"
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        do {
            urlRequest.httpBody = try JSONEncoder().encode(authenticationRequest)
        } catch let error {
            print(error)
            completion(.failure(APIError.encodingProblem))
        }
        _ = URLSession.shared.dataTask(with: urlRequest) {data, response, _ in
            guard let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200, let jsonData = data else {
                completion(.failure(.responseProblem))
                return
            }
            do {
                let registerResponse = try JSONDecoder().decode(AuthenticationResponse.self, from: jsonData)
                let user = User(userId: registerResponse.userId, email: authenticationRequest.email, token: registerResponse.token)
                completion(.success(user))
            } catch let error {
                print(error)
                completion(.failure(APIError.decodingProblem))
            }
            }.resume()
    }
    
    internal func loginUser(with authenticationRequest: AuthenticationRequest,
                       completion: @escaping(Result<User, APIError>)
        -> Void) {
        var urlRequest = URLRequest(url: loginEndpoint)
        urlRequest.httpMethod = "POST"
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        do {
            urlRequest.httpBody = try JSONEncoder().encode(authenticationRequest)
        } catch let error {
            print(error)
            completion(.failure(APIError.encodingProblem))
        }
        _ = URLSession.shared.dataTask(with: urlRequest) {data, response, _ in
            guard let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200, let jsonData = data else {
                completion(.failure(.responseProblem))
                return
            }
            do {
                let loginResponse = try JSONDecoder().decode(AuthenticationResponse.self, from: jsonData)
                let user = User(userId: loginResponse.userId, email: authenticationRequest.email, token: loginResponse.token)
                completion(.success(user))
            } catch let error {
                print(error)
                completion(.failure(APIError.decodingProblem))
            }
            
        }.resume()
    }
    
    internal func authenticate(user: User,
                              completion: @escaping(Result<User, APIError>)
        -> Void) {
        guard let authString = user.authString else { return }
        var urlRequest = URLRequest(url: authenticationEndpoint)
        urlRequest.httpMethod = "POST"
        urlRequest.addValue(authString, forHTTPHeaderField: "Authorization")
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        _ = URLSession.shared.dataTask(with: urlRequest) {data, response, _ in
            guard let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200, let jsonData = data else {
                completion(.failure(.responseProblem))
                return
            }
            do {
                let authenticationResponse = try JSONDecoder().decode(AuthenticationResponse.self, from: jsonData)
                user.setToken(token: authenticationResponse.token)
            } catch let error {
                print(error)
                completion(.failure(APIError.decodingProblem))
            }
            completion(.success(user))
        }.resume()
    }
    
    // ==========================================================================================
    // Statistics
    // ==========================================================================================
    //
    
    internal func openSession(for user: User,
                               completion: @escaping(Result<Session, APIError>)
        -> Void) {
        guard let authString = user.authString else { return }
        var urlRequest = URLRequest(url: sessionStartEndpoint)
        urlRequest.httpMethod = "POST"
        urlRequest.addValue(authString, forHTTPHeaderField: "Authorization")
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        _ = URLSession.shared.dataTask(with: urlRequest) {data, response, _ in
            guard let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200, let jsonData = data else {
                completion(.failure(.responseProblem))
                return
            }
            do {
                let session = try JSONDecoder().decode(Session.self, from: jsonData)
                completion(.success(session))
            } catch let error {
                print(error)
                completion(.failure(APIError.decodingProblem))
            }
        }.resume()
    }
    
    internal func closeSession(for session: Session,
                               from user: User,
                               completion: @escaping(Result<Session, APIError>)
        -> Void) {
        
        guard let authString = user.authString else { return }
        var urlRequest = URLRequest(url: sessionStopEndpoint)
        urlRequest.httpMethod = "POST"
        urlRequest.addValue(authString, forHTTPHeaderField: "Authorization")
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        do {
            urlRequest.httpBody = try JSONEncoder().encode(session)
        } catch let error {
            print(error)
            completion(.failure(APIError.encodingProblem))
        }
        _ = URLSession.shared.dataTask(with: urlRequest) {data, response, _ in
            guard let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200, let _ = data else {
                completion(.failure(.responseProblem))
                return
            }
            completion(.success(session))
        }.resume()
    }
    
    internal func submitNPSRating(npsRating: NPSRating,
                                  from user: User,
                                  completion: @escaping(Result<NPSRating, APIError>)
        -> Void) {
        
        guard let authString = user.authString else { return }
        var urlRequest = URLRequest(url: npsRatingEndpoint)
        urlRequest.httpMethod = "POST"
        urlRequest.addValue(authString, forHTTPHeaderField: "Authorization")
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        do {
            urlRequest.httpBody = try JSONEncoder().encode(npsRating)
        } catch let error {
            print(error)
            completion(.failure(APIError.encodingProblem))
        }
        _ = URLSession.shared.dataTask(with: urlRequest) {data, response, _ in
            guard let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200, let jsonData = data else {
                completion(.failure(.responseProblem))
                return
            }
            if let response = String(data: jsonData, encoding: .utf8), let timestamp = Int(response) {
                npsRating.setRatingTimestamp(timestamp: timestamp)
                completion(.success(npsRating))
            } else {
                completion(.failure(APIError.decodingProblem))
            }
            }.resume()
    }
    
    
    
//    func sendNPSRating(npsRating: Int) {
//        guard let user = self.user, let token = user.token, let userId = user.userId else { return }
//        let authString = "Bearer " + token
//        let newNPSRating: NPSRating = NPSRating(userId: userId, npsRating: npsRating)
//        do {
//            var urlRequest = URLRequest(url: NPSRatingEndpoint)
//            urlRequest.httpMethod = "POST"
//            urlRequest.addValue(authString, forHTTPHeaderField: "Authorization")
//            urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
//            urlRequest.httpBody = try JSONEncoder().encode(newNPSRating)
//            let dataTask = URLSession.shared.dataTask(with: urlRequest) {data, response, _ in
//                guard let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200, let jsonData = data else {
//                    print(APIError.responseProblem)
//                    return
//                }
//                do {
//                    let session = try JSONDecoder().decode(Session.self, from: jsonData)
//                    completion(.success(session))
//                } catch let error {
//                    print(error)
//                    completion(.failure(APIError.decodingProblem))
//                }
//                print("NPS rating has been saved to Server, please implement other measure to control if user has set NPS already!!!!")
//
//            }
//            dataTask.resume()
//        } catch let error {
//            print("NPSEncodingProblem")
//            print(error)
//            print(APIError.encodingProblem)
//        }
//    }
    
    // ==========================================================================================
    // CRUD Methods
    // ==========================================================================================
    //

    private func createOnServer(on endpoint: URL,
                        send request: BackendWriteRequest?,
                        completion: @escaping(Result<Data?, APIError>)
        -> Void) {
        guard let authString = AuthenticationProvider.shared.user?.authString else { return }
        var urlRequest = URLRequest(url: endpoint)
        urlRequest.httpMethod = "POST"
        urlRequest.addValue(authString, forHTTPHeaderField: "Authorization")
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        if let request = request {
            do {
                urlRequest.httpBody = try JSONEncoder().encode(request)
            } catch let error {
                print(error)
                completion(.failure(.encodingProblem))
            }
        }
        _ = URLSession.shared.dataTask(with: urlRequest) {data, response, _ in
            guard let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200, let jsonData = data else {
                completion(.failure(.responseProblem))
                return
            }
            completion(.success((jsonData)))
        }.resume()
    }
    
    private func readFromServer(from endpoint: URL,
              send request: BackendReadRequest?,
              completion: @escaping(Result<Data?, APIError>)
        -> Void)
    {
        guard let authString = AuthenticationProvider.shared.user?.authString else { return }
        var urlRequest = URLRequest(url: endpoint)
        urlRequest.httpMethod = "POST"
        urlRequest.addValue(authString, forHTTPHeaderField: "Authorization")
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        if let request = request {
            do {
                urlRequest.httpBody = try JSONEncoder().encode(request)
            } catch let error {
                print(error)
                completion(.failure(.encodingProblem))
            }
        }
        _ = URLSession.shared.dataTask(with: urlRequest) {data, response, _ in
            guard let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200, let jsonData = data else {
                completion(.failure(.responseProblem))
                return
            }
            completion(.success((jsonData)))
        }.resume()
    }
       
    
    private func update() {
        
    }
    
    private func delete() {
        
    }
    
    
    // ==========================================================================================
    // Get Data Methods
    // ==========================================================================================
    //

    internal func getStashsFromServer(
        by userId: String,
        completion: @escaping(Result<[Stash], APIError>)
        -> Void)
    {
        let stashRequest = BackendReadRequest(userId: userId)
        readFromServer(from: getStashsByUserEndpoint, send: stashRequest, completion: ({ result in
            switch result {
            case .success(let data):
                do {
                    guard let data = data else { return }
                    let stashsFromJSON = try JSONDecoder().decode([StashFromJSON].self, from: data)
                    var stashs: [Stash] = []
                    for stashFromJSON in stashsFromJSON {
                        let stash = Stash(stashFromJSON: stashFromJSON)
                        stashs.append(stash)
                    }
                    completion(.success(stashs))
                } catch let error {
                    print(error)
                    completion(.failure(.decodingProblem))
                }
            case .failure(let error):
                completion(.failure(error))
            }
        }))
    }

    internal func getStashObjectPreviewsFromServer(
        by stashObjectPreviewIds: [String],
        completion: @escaping(Result<[StashObjectPreview], APIError>)
        -> Void)
    {
        
        let stashObjectPreviewRequest = BackendReadRequest(stashObjectPreviewIds: stashObjectPreviewIds)
        readFromServer(from: getStashObjectPreviewsEndpoint, send: stashObjectPreviewRequest, completion: ({ result in
            switch result {
            case .success(let data):
                do {
                    guard let data = data else { return }
                    let stashObjectPreviewsFromJSON = try JSONDecoder().decode([StashObjectPreviewFromJSON].self, from: data)
                    var stashObjectPreviews: [StashObjectPreview] = []
                    for stashObjectPreviewFromJSON in stashObjectPreviewsFromJSON {
                        let stashObjectPreview = StashObjectPreview(stashObjectPreviewFromJSON: stashObjectPreviewFromJSON)
                        stashObjectPreviews.append(stashObjectPreview)
                    }
                    completion(.success(stashObjectPreviews))
                } catch let error {
                    print(error)
                    completion(.failure(.decodingProblem))
                }
            case .failure(let error):
                completion(.failure(error))
            }
        }))
    }
    
    internal func getStashObjectPreviewsFromServer(
    around location: CLLocation,
    withRadius radius: Int,
    completion: @escaping(Result<[StashObjectPreview], APIError>)
    -> Void)
    {
        
        let stashObjectPreviewRequest = BackendReadRequest(fromLocation: location, withRadius: radius)
        readFromServer(from: getStashObjectPreviewsEndpoint, send: stashObjectPreviewRequest, completion: ({ result in
            switch result {
            case .success(let data):
                do {
                    guard let data = data else { return }
                    let stashObjectPreviewsFromJSON = try JSONDecoder().decode([StashObjectPreviewFromJSON].self, from: data)
                    var stashObjectPreviews: [StashObjectPreview] = []
                    for stashObjectPreviewFromJSON in stashObjectPreviewsFromJSON {
                        let stashObjectPreview = StashObjectPreview(stashObjectPreviewFromJSON: stashObjectPreviewFromJSON)
                        stashObjectPreviews.append(stashObjectPreview)
                    }
                    completion(.success(stashObjectPreviews))
                } catch let error {
                    print(error)
                    completion(.failure(.decodingProblem))
                }
            case .failure(let error):
                completion(.failure(error))
            }
        }))
    }
        

        
    
    internal func getStashObjectsFromServer(
        withIds stashObjectIds: [String],
        completion: @escaping(Result<[StashObject], APIError>)
        -> Void)
    {
        
        let stashObjectRequest = BackendReadRequest(stashObjectIds: stashObjectIds)
        readFromServer(from: getStashObjectsEndpoint, send: stashObjectRequest, completion: ({ result in
            switch result {
            case .success(let data):
                do {
                    guard let data = data else { return }
                    let stashObjectsFromJSON = try JSONDecoder().decode([StashObjectFromJSON].self, from: data)
                    var stashObjects: [StashObject] = []
                    for stashObjectFromJSON in stashObjectsFromJSON {
                        let stashObject = StashObject(stashObjectFromJSON: stashObjectFromJSON)
                        stashObjects.append(stashObject)
                    }
                    completion(.success(stashObjects))
                } catch let error {
                    print(error)
                    completion(.failure(.decodingProblem))
                }
            case .failure(let error):
                completion(.failure(error))
            }
        }))
    }



    internal func getStashsFromServer(
        withIds stashIds: [String],
        completion: @escaping(Result<[Stash], APIError>)
        -> Void)
    {
        
        let stashRequest = BackendReadRequest(stashIds: stashIds)
        readFromServer(from: getStashsByIdEndpoint, send: stashRequest, completion: ({ result in
            switch result {
            case .success(let data):
                do {
                    guard let data = data else { return }
                    let stashsFromJSON = try JSONDecoder().decode([StashFromJSON].self, from: data)
                    var stashs: [Stash] = []
                    for stashFromJSON in stashsFromJSON {
                        let stash = Stash(stashFromJSON: stashFromJSON)
                        stashs.append(stash)
                    }
                    completion(.success(stashs))
                } catch let error {
                    print(error)
                    completion(.failure(.decodingProblem))
                }
            case .failure(let error):
                completion(.failure(error))
            }
        }))
    }
    
    
    
    internal func getImagesFromServer(
        withIds imageIds: [String],
        completion: @escaping(Result<[Image], APIError>)
        -> Void)
    {
        let imageRequest = BackendReadRequest(imageIds: imageIds)
        readFromServer(from: getImagesEndpoint, send: imageRequest, completion: ({ result in
            switch result {
            case .success(let data):
                do {
                    guard let data = data else { return }
                    let imagesFromJSON = try JSONDecoder().decode([ImageFromJSON].self, from: data)
                    var images: [Image] = []
                    for imageFromJSON in imagesFromJSON {
                        let image = Image(imageFromJSON: imageFromJSON)
                        
                        images.append(image)
                    }
                    completion(.success(images))
                } catch let error {
                    print(error)
                    completion(.failure(.decodingProblem))
                }
            case .failure(let error):
                completion(.failure(error))
            }
        }))
    }



    // ==========================================================================================
    // Post Data Methods
    // ==========================================================================================
    //

    internal func postToServer(
        stashObject: StashObject,
        completion: @escaping(Result<StashObject, APIError>)
        -> Void)
    {
        
        let stashObjectRequest = BackendWriteRequest(stashObject: stashObject)
        createOnServer(on: postStashObjectEndpoint, send: stashObjectRequest, completion: ({ result in
            switch result {
            case .success(let data):
                do {
                    guard let data = data else { return }
                    let response = try JSONDecoder().decode(BackendWriteResponse.self, from: data)
                    stashObject.update(stashObjectResponse: response)
                    completion(.success(stashObject))
                } catch let error {
                    print(error)
                    completion(.failure(.decodingProblem))
                }
            case .failure(let error):
                completion(.failure(error))
            }
        }))
    }
    
    internal func postToServer(
        stash: Stash,
        completion: @escaping(Result<Stash, APIError>)
        -> Void)
    {
        
        let stashObjectRequest = BackendWriteRequest(stash: stash)
        createOnServer(on: postStashEndpoint, send: stashObjectRequest, completion: ({ result in
            switch result {
            case .success(let data):
                do {
                    guard let data = data else { return }
                    let response = try JSONDecoder().decode(BackendWriteResponse.self, from: data)
                    stash.update(stashResponse: response)
                    completion(.success(stash))
                } catch let error {
                    print(error)
                    completion(.failure(.decodingProblem))
                }
            case .failure(let error):
                completion(.failure(error))
            }
        }))
    }
}
