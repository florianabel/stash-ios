//
//  DataContainerSingleton.swift
//  Stash
//
//  Created by Florian on 7/8/19.
//  Copyright © 2019 Florian Abel. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

class DataSingleton {
    
    
    // ==========================================================================================
    // Variables and Constants
    // ==========================================================================================
    //
    
    // Singleton instantiation
    static let shared = DataSingleton()
    
    // Delegate
    private weak var delegate: dataInteraction?
    
    // Data storage
    var stashObjectPreviewContainer: [StashObjectPreview] = []
    var stashObjectContainer: [StashObject] = []
    var stashContainer: [Stash] = []
    var myStashContainer: [Stash] = []
    var imageContainer: [Image] = []
    var feedItemContainer: [FeedItem] = []
    
    
    
    // ==========================================================================================
    // Initialization
    // ==========================================================================================
    //
    private init() {
        getMyStashs()
    }
    
    
    // ==========================================================================================
    // Setup Entrance Points for Controller
    // ==========================================================================================
    //
    func setDelegate(_ delegate: dataInteraction) {
        self.delegate = delegate
    }

    
    // ==========================================================================================
    // Functional Entrance Points for Controller
    // ==========================================================================================
    //
    func updateMyStashs() {
        getMyStashs()
    }
    
    func updateData () {
        guard let currentLocation = LocationProvider.shared.currentLocation else { return }
        getStashObjectPreviews(around: currentLocation, withRadius: 5000)
    }
    
    func updateData (location: CLLocation) {
        getStashObjectPreviews(around: location, withRadius: 5000)
    }
    
    func updateDistance () {
        guard let location = LocationProvider.shared.currentLocation else { return }
        _ = feedItemContainer.map { $0.calculateDistance(from: location)}
        updateView()
    }

    func updateDistance (from location: CLLocation) {
        _ = feedItemContainer.map { $0.calculateDistance(from: location)}
        updateView()
    }
    
    func emptyContainer() {
        self.stashObjectPreviewContainer.removeAll()
        self.stashObjectContainer.removeAll()
        self.stashContainer.removeAll()
        self.myStashContainer.removeAll()
        self.imageContainer.removeAll()
        self.feedItemContainer.removeAll()
    }
    
    // ==========================================================================================
    // Stash by User methods
    // ==========================================================================================
    //
    
    private func getMyStashs() {
        guard let userId = AuthenticationProvider.shared.user?.userId else { return }
        getStashs(by: userId, completion: ({ result in
            switch result {
            case .success(_):
                let myStashs = self.filterStashContainer(by: userId)
                self.mergeMyStashContainer(with: myStashs)
                self.updateView()
            case .failure(let error):
                print("Could not get stashs from the server due to: ")
                print(error)
            }
        }))
    }
    
    private func getStashs(by userId: String,
                   completion: @escaping(Result<Bool, APIError>)
        -> Void)
    {
        ConnectionProvider.shared.getStashsFromServer(by: userId, completion: ({ result in
            switch result {
            case .success(let stashs):
                let mergedStashs = self.mergeStashContainer(with: stashs)
                
                var stashObjectIdsToGet: [String] = []
                _ = mergedStashs.map{
                        if let stashObjectId = $0.stashObjectId {
                            stashObjectIdsToGet.append(stashObjectId)
                        }
                    }
                self.getStashObjectPreviews(by: stashObjectIdsToGet)
                
                _ = mergedStashs.map {
                    let imageIds = $0.imageIds
                    if imageIds.count > 0 {
                        self.selectImagesToGet(with: imageIds)
                    }
                }
                completion(.success(true))
            case .failure(let error):
                print("Could not get stashs from the server due to: ")
                print(error)
                completion(.failure(error))
            }
        }))
    }
    
    private func getStashObjectPreviews(by stashObjectPreviewIds: [String]) {
        ConnectionProvider.shared.getStashObjectPreviewsFromServer(by: stashObjectPreviewIds, completion: ({ result in
            switch result {
            case .success(let stashObjectPreviews):
                self.selectStashObjectsToGet(from: stashObjectPreviews)
            case .failure(let error):
                print("Could not get stashObject previews from the server due to: ")
                print(error)
            }
        }))
    }
    
    // ==========================================================================================
    // Stashobjectpreview methods
    // ==========================================================================================
    //
    
    private func getStashObjectPreviews(around location: CLLocation, withRadius radius: Int) {
        ConnectionProvider.shared.getStashObjectPreviewsFromServer(around: location, withRadius: radius, completion: ({ result in
            switch result {
            case .success(let stashObjectPreviews):
                self.selectStashObjectsToGet(from: stashObjectPreviews)
            case .failure(let error):
                print("Could not get stashObject previews from the server due to: ")
                print(error)
            }
        }))
    }
    
    private func selectStashObjectsToGet (from stashObjectPreviews: [StashObjectPreview]) {
        
        let mergedStashObjectPreviews = mergeStashObjectPreviewContainer(with: stashObjectPreviews)
        
        let filteredStashObjectIdsByStashScore = filterByStashScore(stashObjectPreviews: mergedStashObjectPreviews)
        if filteredStashObjectIdsByStashScore.count > 0 {
            getStashObjects(with: filteredStashObjectIdsByStashScore)
        }
    }
    
    private func getStashObjects(with stashObjectIds: [String]) {
        ConnectionProvider.shared.getStashObjectsFromServer(withIds: stashObjectIds, completion: ({ result in
            switch result {
            case .success(let stashObjects):
                let mergedStashObjects = self.mergeStashObjectContainer(with: stashObjects)
                for stashObject in mergedStashObjects {
                    let stashIds = stashObject.stashIds
                    if stashIds.count > 0 {
                        self.getStashs(with: stashIds)
                    }
                    let imageIds = stashObject.imageIds
                    if imageIds.count > 0 {
                        self.selectImagesToGet(with: imageIds)
                    }
                }
                self.createFeedItems(stashObjects: mergedStashObjects)
            case .failure(let error):
                print("Could not get stashObjects from the server due to: ")
                print(error)
            }
        }))
    }
    
    private func getStashs(with stashIds: [String]) {
        ConnectionProvider.shared.getStashsFromServer(withIds: stashIds, completion: ({ result in
            switch result {
            case .success(let stashs):
                let mergedStashs = self.mergeStashContainer(with: stashs)
                for stash in mergedStashs {
                    let imageIds = stash.imageIds
                    if imageIds.count > 0 {
                        self.selectImagesToGet(with: imageIds)
                    }
                }
            case .failure(let error):
                print("Could not get stashs from the server due to: ")
                print(error)
            }
        }))
    }
    
    private func selectImagesToGet(with imageIds: [String]) {
        var imagesToGet: [String] = []
        for imageId in imageIds {
            if !self.imageContainer.contains(where: {$0.imageId == imageId}) {
                imagesToGet.append(imageId)
            }
        }
        getImages(imageIds: imagesToGet)
    }
    
    private func getImages(imageIds: [String]) {
        ConnectionProvider.shared.getImagesFromServer(withIds: imageIds, completion: ({ result in
            switch result {
            case .success(let images):
                self.addNew(images: images)
            case .failure(let error):
                print("Could not get images from the server due to: ")
                print(error)
            }
        }))
    }
    
    
    
    private func addNew(images: [Image]) {
        for image in images {
            self.imageContainer.append(image)
            
            guard let stashObjectId = image.stashObjectId, let stashObject = self.stashObjectContainer.first(where: {$0.stashObjectId == stashObjectId}), let feedItem = self.feedItemContainer.first(where: {$0.stashObject.stashObjectId == stashObjectId}) else { return }
            
            stashObject.addImage(image: image)
            if let stashId = image.stashId, let stash = self.stashContainer.first(where: {$0.stashId == stashId}) {
                stash.addImage(image: image)
                feedItem.addImage(image: image)
            } else {
                feedItem.setMainImage(image: image)
            }
        }
        updateView()
    }
    
    private func createFeedItems (stashObjects: [StashObject]) {

        
        _ = stashObjects.map {
            stashObject in if let feedItem = self.feedItemContainer.first(where: {$0.stashObject.stashObjectId == stashObject.stashObjectId}) {
                feedItem.update(stashObject: stashObject)
            } else {
                let newFeedItem = FeedItem(stashObject: stashObject)
                feedItemContainer.append(newFeedItem)
            }
        }
        sortFeed()
    }
    
    private func sortFeed () {
        updateDistance()
        self.feedItemContainer.sort(by: {$0.distance < $1.distance})
        updateView()
    }
    
    private func updateView () {
        self.delegate?.updateView()
    }
    
    // ==========================================================================================
    // Post Methods
    // ==========================================================================================
    //
    
    func addNew(stashObject: StashObject,
                completion: @escaping(Result<Bool, APIError>)
        -> Void)
    {
        ConnectionProvider.shared.postToServer(stashObject: stashObject, completion: ({ result in
            switch result {
            case .success(let newStashObject):
                self.stashObjectContainer.append(newStashObject)
                self.createFeedItems(stashObjects: [newStashObject])
                if newStashObject.images.count > 0 {
                    let images = newStashObject.images
                    self.addNew(images: images)
                }
                completion(.success(true))
            case .failure(let error):
                print("Could not add stashObject due to: ")
                print(error)
                completion(.failure(error))
            }
        }))
    }
    
    func addNew(stash: Stash,
                completion: @escaping(Result<Bool, APIError>)
        -> Void)
    {
        ConnectionProvider.shared.postToServer(stash: stash, completion: ({ result in
            switch result {
            case .success(let newStash):
                self.stashContainer.append(newStash)
                if let stashObject = self.stashObjectContainer.first(where: {$0.stashObjectId == newStash.stashObjectId}) {
                    stashObject.addStash(stash: newStash)
                }
                if newStash.images.count > 0 {
                    let images = newStash.images
                    self.addNew(images: images)
                }
                completion(.success(true))
            case .failure(let error):
                print("Could not add stashObject due to: ")
                print(error)
                completion(.failure(error))
            }
        }))
    }
    
    
    // ==========================================================================================
    // Helper Methods - Stashs by User
    // ==========================================================================================
    //
    
    private func filterStashContainer(by userId: String) -> [Stash] {
        return stashContainer.filter({
            $0.userId == userId
        })
    }
    
    private func mergeMyStashContainer(with myStashs: [Stash]) {
        _ = myStashs.map {
            myStash in if let stash = self.myStashContainer.first(where: {$0.stashId == myStash.stashId}) {
                stash.update(stash: myStash)
            } else {
                self.myStashContainer.append(myStash)
            }
        }
    }
    

    
    // ==========================================================================================
    // Helper Methods - StashObjectPreviews
    // ==========================================================================================
    //
    
    private func mergeStashObjectPreviewContainer(with stashObjectPreviews: [StashObjectPreview]) -> [StashObjectPreview] {
        var newStashObjectPreviews: [StashObjectPreview] = []
        _ = stashObjectPreviews.map({
            newStashObjectPreview in if let stashObjectPreview = self.stashObjectPreviewContainer.first(where: {$0.stashObjectId == newStashObjectPreview.stashObjectId}) {
                if  stashObjectPreview.lastUpdatedTimestamp == newStashObjectPreview.lastUpdatedTimestamp {
                    stashObjectPreview.update(stashObjectPreview: newStashObjectPreview)
                    newStashObjectPreviews.append(newStashObjectPreview)
                }
            } else {
                self.stashObjectPreviewContainer.append(newStashObjectPreview)
                newStashObjectPreviews.append(newStashObjectPreview)
            }
        })
        return newStashObjectPreviews
    }
        
    
    private func filterByStashScore(stashObjectPreviews: [StashObjectPreview]) -> [String] {
        var stashObjectIds: [String] = []
        let sortedStashObjectPreviews = stashObjectPreviews.sorted(by: { $0.stashScore > $1.stashScore })
        
        stashObjectIds = sortedStashObjectPreviews.prefix(30).map { $0.stashObjectId }
        return stashObjectIds
    }
    
    // ==========================================================================================
    // Helper Methods - StashObjects
    // ==========================================================================================
    //
    
    private func mergeStashObjectContainer(with stashObjects: [StashObject]) -> [StashObject] {
        var stashObjectsToMerge: [StashObject] = []
        
        let stashObjectsToAdd = filterNew(stashObjects: stashObjects)
        stashObjectsToMerge.append(contentsOf: stashObjectsToAdd)
        
        let stashObjectsToUpdate = filterUpdated(stashObjects: stashObjects)
        stashObjectsToMerge.append(contentsOf: stashObjectsToUpdate)
        
        self.stashObjectContainer = self.stashObjectContainer.filter({
            updatedStashObject in !stashObjectsToUpdate.contains(where: {
                $0.stashObjectId == updatedStashObject.stashObjectId
            })
        })
        
        addToStashObjectContainer(from: stashObjectsToMerge)
        return stashObjectsToMerge
    }
    
    private func filterNew(stashObjects: [StashObject]) -> [StashObject] {
        return stashObjects.filter({
            stashObject in !self.stashObjectContainer.contains(where: {
                $0.stashObjectId == stashObject.stashObjectId
            })
        })
    }
    
    private func filterUpdated(stashObjects: [StashObject]) -> [StashObject] {
        return stashObjects.filter({
            stashObject in self.stashObjectContainer.contains(where: {
                $0.stashObjectId == stashObject.stashObjectId &&
                    $0.lastUpdatedTimestamp < stashObject.lastUpdatedTimestamp
            })
        })
    }
    
    private func addToStashObjectContainer(from stashObjects: [StashObject]) {
        self.stashObjectContainer.append(contentsOf: stashObjects)
    }
    
    // ==========================================================================================
    // Helper Methods - Stashs
    // ==========================================================================================
    //
    private func mergeStashContainer(with stashs: [Stash]) -> [Stash] {
        var stashsToMerge: [Stash] = []
    
        let stashsToAdd = filterNew(stashs: stashs)
        stashsToMerge.append(contentsOf: stashsToAdd)
    
        let stashsToUpdate = filterUpdated(stashs: stashs)
        stashsToMerge.append(contentsOf: stashsToUpdate)
    
        self.stashContainer = self.stashContainer.filter({
            updatedStash in !stashsToUpdate.contains(where: {
                $0.stashId == updatedStash.stashId
            })
        })
    
        addToStashContainer(from: stashsToMerge)
        return stashsToMerge
    }
    
    private func filterNew(stashs: [Stash]) -> [Stash] {
        return stashs.filter({
            stash in !self.stashContainer.contains(where: {
                $0.stashId == stash.stashId
            })
        })
    }
    
    private func filterUpdated(stashs: [Stash]) -> [Stash] {
        return stashs.filter({
            stash in self.stashContainer.contains(where: {
                $0.stashId == stash.stashId &&
                    $0.lastUpdatedTimestamp < stash.lastUpdatedTimestamp
            })
        })
    }
    
    private func addToStashContainer(from stashs: [Stash]) {
        self.stashContainer.append(contentsOf: stashs)
    }
}

// INSPIRATION TO PUT MORE THAN ONE THING IN THE ARRAY CONTAINS METHOD!!!!!
// CHECK IF FILTER HELPER METHODS CAN BE OPTIMIZED WITH THAT
// ALSO CHECK IF "CONTAINS" IS BETTER SUITED SOMEWHERE!!!!
//                if !self.images.contains(where: {$0.imageId == imageJSON.imageId}) {
//                    self.images.append(image)
//                    self.addImage(image: image)
//                }
