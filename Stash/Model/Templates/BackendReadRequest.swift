//
//  BackendRequest.swift
//  Stash
//
//  Created by Florian on 8/22/19.
//  Copyright © 2019 Florian Abel. All rights reserved.
//

import Foundation
import CoreLocation

class BackendReadRequest: Encodable {
    var userId: String?
    var stashObjectPreviewIds: [String]?
    var stashObjectIds: [String]?
    var stashIds: [String]?
    var imageIds: [String]?
    var latitude: Double?
    var longitude: Double?
    var distanceFromLocation: Int?
    
    init() {
        
    }
    
    init(userId: String) {
        self.userId = userId
    }
    
    init(fromLocation location: CLLocation, withRadius radius: Int) {
        self.latitude = location.coordinate.latitude
        self.longitude = location.coordinate.longitude
        self.distanceFromLocation = radius
    }
    
    init(stashObjectPreviewIds: [String]) {
        self.stashObjectPreviewIds = stashObjectPreviewIds
    }
    
    init(stashObjectIds: [String]) {
        self.stashObjectIds = stashObjectIds
    }
    
    init(stashIds: [String]) {
        self.stashIds = stashIds
    }
    
    init(imageIds: [String]) {
        self.imageIds = imageIds
    }
}









