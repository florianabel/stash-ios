//
//  BackendCreateWrite.swift
//  Stash
//
//  Created by Florian on 8/23/19.
//  Copyright © 2019 Florian Abel. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

class BackendWriteRequest: Encodable{
    var stashObjectId: String?
    var message: String?
    var type: String?
    var title: String?
    var subtitle: String?
    var description: String?
    var latitude: Double?
    var longitude: Double?
    var creationTimestamp: Int?
    var expirationTimestamp: Int?
    var imageBase64: String?
    var tags: String?
    
    init(stashObject: StashObject) {
        
    }
    
    init(stash: Stash) {
        self.latitude = stash.latitude
        self.longitude = stash.longitude
        if stash.stashObjectId != nil {
            self.stashObjectId  = stash.stashObjectId
        }
        if stash.message != nil {
            self.message        = stash.message
        }
        if stash.images != nil {
            
        }
        
    }
    init(type: String, title: String, subtitle: String, description: String, location: CLLocation, image: UIImage?) {
        self.type = type
        self.title = title
        self.subtitle = subtitle
        self.description = description
        self.latitude = location.coordinate.latitude
        self.longitude = location.coordinate.longitude
        let currentTime = Date()
        let expirationTime = Date(timeInterval: TimeInterval(integerLiteral: 60 * 60 * 24 * 7), since: currentTime)
        self.creationTimestamp = Int(currentTime.timeIntervalSince1970)
        self.expirationTimestamp = Int(expirationTime.timeIntervalSince1970)
        if let image = image {
            let imageData = image.jpegData(compressionQuality: 0.4)
            self.imageBase64 = (imageData?.base64EncodedString(options: .lineLength64Characters))!
        }
        
    }
    
    init(location: CLLocation) {
        self.latitude = location.coordinate.latitude
        self.longitude = location.coordinate.longitude
        let currentTime = Date()
        let expirationTime = Date(timeInterval: TimeInterval(integerLiteral: 60 * 60 * 24 * 3), since: currentTime)
        self.creationTimestamp = Int(currentTime.timeIntervalSince1970)
        self.expirationTimestamp = Int(expirationTime.timeIntervalSince1970)
    }
    
    init(stashObjectId: String, location: CLLocation) {
        self.stashObjectId = stashObjectId
        self.latitude = location.coordinate.latitude
        self.longitude = location.coordinate.longitude
        let currentTime = Date()
        let expirationTime = Date(timeInterval: TimeInterval(integerLiteral: 60 * 60 * 24 * 3), since: currentTime)
        self.creationTimestamp = Int(currentTime.timeIntervalSince1970)
        self.expirationTimestamp = Int(expirationTime.timeIntervalSince1970)
    }
    
    init(stashObjectId: String, message: String?, location: CLLocation, image: UIImage?) {
        self.stashObjectId = stashObjectId
        self.message = message
        self.latitude = location.coordinate.latitude
        self.longitude = location.coordinate.longitude
        let currentTime = Date()
        let expirationTime = Date(timeInterval: TimeInterval(integerLiteral: 60 * 60 * 24 * 3), since: currentTime)
        self.creationTimestamp = Int(currentTime.timeIntervalSince1970)
        self.expirationTimestamp = Int(expirationTime.timeIntervalSince1970)
        if let image = image {
            let imageData = image.jpegData(compressionQuality: 0.4)
            self.imageBase64 = (imageData?.base64EncodedString(options: .lineLength64Characters))!
        }
    }
}
