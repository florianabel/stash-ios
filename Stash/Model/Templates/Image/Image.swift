//
//  Image.swift
//  Stash
//
//  Created by Florian on 8/13/19.
//  Copyright © 2019 Florian Abel. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

class Image {
    var imageId: String?
    var image: UIImage?
    var stashId: String?
    var stashObjectId: String?
    var latitude: Double
    var longitude: Double
    var creationTimestamp: Int?
    var lastUpdatedTimestamp: Int?
    var userId: String?
    
    init(imageFromJSON: ImageFromJSON) {
        self.imageId = imageFromJSON.imageId
        self.latitude = imageFromJSON.latitude
        self.longitude = imageFromJSON.longitude
        self.creationTimestamp = imageFromJSON.creationTimestamp
        self.lastUpdatedTimestamp = imageFromJSON.lastUpdatedTimestamp
        self.userId = imageFromJSON.userId
        let decodedData = NSData(base64Encoded: imageFromJSON.imageBase64, options: [])
        if let data = decodedData {
            self.image = UIImage(data: data as Data)
        } else {
            print("error with decodedData")
        }
        if let stashObjectId = imageFromJSON.stashObjectId {
            setStashObjectId(stashObjectId: stashObjectId)
        }
        if let stashId = imageFromJSON.stashId {
            setStashId(stashId: stashId)
        }
    }
    
    
    init(imageId: String, imageBase64: String, latitude: Double, longitude: Double, timestamp: Int, userId: String) {
        self.imageId = imageId
        self.latitude = latitude
        self.longitude = longitude
        self.creationTimestamp = timestamp
        self.lastUpdatedTimestamp = timestamp
        self.userId = userId
        let decodedData = NSData(base64Encoded: imageBase64, options: [])
        if let data = decodedData {
            self.image = UIImage(data: data as Data)
        } else {
            print("error with decodedData")
        }
    }
    
    init(image: UIImage, location: CLLocation) {
        self.image = image
        self.latitude = location.coordinate.latitude
        self.longitude = location.coordinate.longitude
    }
    
    func setStashId(stashId: String) {
        self.stashId = stashId
    }

    func setStashObjectId(stashObjectId: String) {
        self.stashObjectId = stashObjectId
    }
}
