//
//  ImageFromJSON.swift
//  Stash
//
//  Created by Florian on 8/13/19.
//  Copyright © 2019 Florian Abel. All rights reserved.
//

import Foundation
import UIKit

class ImageFromJSON: Decodable {
    var imageId: String
    var imageBase64: String
    var stashId: String?
    var stashObjectId: String?
    var latitude: Double
    var longitude: Double
    var creationTimestamp: Int
    var lastUpdatedTimestamp: Int
    var userId: String
}
