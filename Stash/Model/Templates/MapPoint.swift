//
//  MapPoint.swift
//  Stash
//
//  Created by Florian on 8/19/19.
//  Copyright © 2019 Florian Abel. All rights reserved.
//

import Foundation
import MapKit

class MapPoint: NSObject, MKAnnotation {
    let title: String?
    let subtitle: String?
    let type: String
    let coordinate: CLLocationCoordinate2D
    var isHidden: Bool = false
    
    
    
    init(title: String, subtitle: String, type: String, coordinate: CLLocationCoordinate2D) {
        self.title = title
        self.subtitle = subtitle
        self.type = type
        self.coordinate = coordinate
        super.init()
    }
}
