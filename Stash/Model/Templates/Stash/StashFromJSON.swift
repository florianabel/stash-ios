//
//  StashFromJSON.swift
//  Stash
//
//  Created by Florian on 7/17/19.
//  Copyright © 2019 Florian Abel. All rights reserved.
//

import Foundation

struct StashFromJSON: Decodable{
    var stashId: String
    var stashObjectId: String
    var userId: String
    var message: String?
    var latitude: Double
    var longitude: Double
    var creationTimestamp: Int
    var lastUpdatedTimestamp: Int
    var expirationTimestamp: Int
    var imageIds: [String]?
}
