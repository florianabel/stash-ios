//
//  StashRequest.swift
//  Stash
//
//  Created by Florian on 8/13/19.
//  Copyright © 2019 Florian Abel. All rights reserved.
//

import Foundation

class StashRequest: Encodable{
    var stashIds: [String]?

    init(stashIds: [String]){
        self.stashIds = stashIds
    }
}
