//
//  StashToJSON.swift
//  Stash
//
//  Created by Florian on 7/18/19.
//  Copyright © 2019 Florian Abel. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

class StashToJSON: Encodable{
    var stashObjectId: String?
    var message: String?
    var latitude: Double
    var longitude: Double
    var creationTimestamp: Int
    var expirationTimestamp: Int
    var imageBase64: String?
    var tags: String?
    
    
    init(location: CLLocation) {
        self.latitude = location.coordinate.latitude
        self.longitude = location.coordinate.longitude
        let currentTime = Date()
        let expirationTime = Date(timeInterval: TimeInterval(integerLiteral: 60 * 60 * 24 * 3), since: currentTime)
        self.creationTimestamp = Int(currentTime.timeIntervalSince1970)
        self.expirationTimestamp = Int(expirationTime.timeIntervalSince1970)
    }
    
    init(stashObjectId: String, location: CLLocation) {
        self.stashObjectId = stashObjectId
        self.latitude = location.coordinate.latitude
        self.longitude = location.coordinate.longitude
        let currentTime = Date()
        let expirationTime = Date(timeInterval: TimeInterval(integerLiteral: 60 * 60 * 24 * 3), since: currentTime)
        self.creationTimestamp = Int(currentTime.timeIntervalSince1970)
        self.expirationTimestamp = Int(expirationTime.timeIntervalSince1970)
    }
    
    init(stashObjectId: String, message: String?, location: CLLocation, image: UIImage?) {
        self.stashObjectId = stashObjectId
        self.message = message
        self.latitude = location.coordinate.latitude
        self.longitude = location.coordinate.longitude
        let currentTime = Date()
        let expirationTime = Date(timeInterval: TimeInterval(integerLiteral: 60 * 60 * 24 * 3), since: currentTime)
        self.creationTimestamp = Int(currentTime.timeIntervalSince1970)
        self.expirationTimestamp = Int(expirationTime.timeIntervalSince1970)
        if let image = image {
            let imageData = image.jpegData(compressionQuality: 0.4)
            self.imageBase64 = (imageData?.base64EncodedString(options: .lineLength64Characters))!
        }
    }
    
    
}
