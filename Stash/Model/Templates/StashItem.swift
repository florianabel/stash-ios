//
//  StashItem.swift
//  Stash
//
//  Created by Florian on 8/20/19.
//  Copyright © 2019 Florian Abel. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

class StashItem {
    
    var stashItemId: UUID
    var stashObject: StashObject
    var stash: Stash?
    var title: String
    var subtitle: String
    var description: String
    var category: String
    var location: CLLocation
    var distance: CLLocationDistance
    var stashs: [Stash] = []
    var images: [Image] = []
    var isFrontView: Bool = true
    var starred: Bool
    
    init(stashObject: StashObject) {
        self.stashObject = stashObject
        self.stashItemId = UUID()
        self.title = stashObject.title
        self.subtitle = stashObject.subtitle
        self.description = stashObject.description
        self.category = stashObject.type
        self.location = stashObject.location
        self.distance = 0
        self.starred = false
        self.calculateDistance()
    }
    
    func calculateDistance() {
        if let currentLocation = LocationProvider.shared.currentLocation {
            let distance = self.location.distance(from: currentLocation)
            self.distance = distance
        }
    }
    
    func setImage(image: Image) {
        self.images.append(image)
    }
    
    func setStash(stash: Stash) {
        self.stashs.append(stash)
    }
    
    func starItem() {
        if starred {
            starred = false
        } else if !starred {
            starred = true
        }
    }
}
