//
//  StashObject.swift
//  Stash
//
//  Created by Florian on 8/5/19.
//  Copyright © 2019 Florian Abel. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

class StashObject {
    
    var stashObjectId: String?
    var type: String
    var owner: String?
    var title: String
    var subtitle: String
    var description: String
    var location: CLLocation
    var distance: CLLocationDistance
    var stashScore: Int?
    var creationTimestamp: Int?
    var lastUpdatedTimestamp: Int
    var expirationTimestamp: Int?
    var images: [Image] = []
    var stashs: [Stash] = []
    var imageIds: [String] = []
    var stashIds: [String] = []
    var status: String?
    
    
    init(stashObjectFromJSON: StashObjectFromJSON) {
        self.stashObjectId = stashObjectFromJSON.stashObjectId
        self.type = stashObjectFromJSON.type
        self.owner = stashObjectFromJSON.owner
        self.title = stashObjectFromJSON.title
        self.subtitle = stashObjectFromJSON.subtitle
        self.description = stashObjectFromJSON.description
        self.location = CLLocation(latitude: stashObjectFromJSON.latitude, longitude: stashObjectFromJSON.longitude)
        self.distance = 0
        self.stashScore = stashObjectFromJSON.stashScore
        self.creationTimestamp = stashObjectFromJSON.creationTimestamp
        self.lastUpdatedTimestamp = stashObjectFromJSON.lastUpdatedTimestamp
        self.expirationTimestamp = stashObjectFromJSON.expirationTimestamp
        self.imageIds = stashObjectFromJSON.imageIds
        self.stashIds = stashObjectFromJSON.stashIds
        self.status = stashObjectFromJSON.status
        self.calculateDistance()
    }
    
    init(type: String, title: String, subtitle: String, description: String, location: CLLocation, images: [Image]) {
        self.type = type
        self.title = title
        self.subtitle = subtitle
        self.description = description
        self.location = location
        if self.images.count > 0 {
            self.images = images
        }
        self.distance = 0
        self.lastUpdatedTimestamp = 0
    }
    
    func update(stashObject: StashObject) {
        
    }
    
    func update(stashObjectResponse: BackendWriteResponse) {
        
    }
    
    init(stashObjectId: String, type: String, owner: String, title: String, subtitle: String, description: String, latitude: Double, longitude: Double, stashScore: Int, creationTimestamp: Int,lastUpdatedTimestamp: Int, expirationTimestamp: Int, imageIds: [String], stashIds: [String], status: String) {
        self.stashObjectId = stashObjectId
        self.type = type
        self.owner = owner
        self.title = title
        self.subtitle = subtitle
        self.description = description
        self.location = CLLocation(latitude: latitude, longitude: longitude)
        self.distance = 0
        self.stashScore = stashScore
        self.creationTimestamp = creationTimestamp
        self.lastUpdatedTimestamp = lastUpdatedTimestamp
        self.expirationTimestamp = expirationTimestamp
        self.imageIds = imageIds
        self.stashIds = stashIds
        self.status = status
        self.calculateDistance()
    }
    
    func calculateDistance() {
        if let currentLocation = LocationProvider.shared.currentLocation {
            let distance = self.location.distance(from: currentLocation)
            self.distance = distance
        }
    }
    
    func addStashId(stashId: String) {
        self.stashIds.append(stashId)
    }
    
    func addStash(stash: Stash) {
        self.stashs.append(stash)
    }
    
    func addImage(image: Image) {
        self.images.append(image)
    }
    
    func updateStashScore(stashScore: Int) {
        self.stashScore = stashScore
    }
}
