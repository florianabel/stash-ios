//
//  StashObjectToJSON.swift
//  Stash
//
//  Created by Florian on 7/30/19.
//  Copyright © 2019 Florian Abel. All rights reserved.
//

import Foundation

struct StashObjectFromJSON: Decodable{
    var stashObjectId: String
    var owner: String
    var type: String
    var title: String
    var subtitle: String
    var description: String
    var imageIds: [String]
    var latitude: Double
    var longitude: Double
    var stashScore: Int
    var creationTimestamp: Int
    var lastUpdatedTimestamp: Int
    var expirationTimestamp: Int
    var stashIds: [String]
    var status: String
}
