//
//  StashObjectResponse.swift
//  Stash
//
//  Created by Florian on 8/16/19.
//  Copyright © 2019 Florian Abel. All rights reserved.
//

import Foundation

struct StashObjectResponse: Decodable{
    var stashObjectId: String
    var imageIds: [String]?
    var stashScore: Int
}
