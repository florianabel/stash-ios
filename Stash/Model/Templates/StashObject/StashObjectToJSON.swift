//
//  StashObjectToJSON.swift
//  Stash
//
//  Created by Florian on 8/5/19.
//  Copyright © 2019 Florian Abel. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

class StashObjectToJSON: Encodable{
    var type: String
    var title: String
    var subtitle: String
    var description: String
    var latitude: Double
    var longitude: Double
    var creationTimestamp: Int
    var expirationTimestamp: Int
    var imageBase64: String?
    
//    var stashObjectId: String?
//    var message: String?
//    var latitude: Double
//    var longitude: Double
//    var creationTimestamp: Int
//    var expirationTimestamp: Int
//    var imageBase64: String?
//    var tags: String?
//
    init(type: String, title: String, subtitle: String, description: String, location: CLLocation, image: UIImage?) {
        self.type = type
        self.title = title
        self.subtitle = subtitle
        self.description = description
        self.latitude = location.coordinate.latitude
        self.longitude = location.coordinate.longitude
        let currentTime = Date()
        let expirationTime = Date(timeInterval: TimeInterval(integerLiteral: 60 * 60 * 24 * 7), since: currentTime)
        self.creationTimestamp = Int(currentTime.timeIntervalSince1970)
        self.expirationTimestamp = Int(expirationTime.timeIntervalSince1970)
        if let image = image {
            let imageData = image.jpegData(compressionQuality: 0.4)
            self.imageBase64 = (imageData?.base64EncodedString(options: .lineLength64Characters))!
        }
        
    }
}
