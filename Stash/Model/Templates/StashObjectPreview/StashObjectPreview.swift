//
//  StashObjectPreview.swift
//  Stash
//
//  Created by Florian on 8/13/19.
//  Copyright © 2019 Florian Abel. All rights reserved.
//

import Foundation
import CoreLocation

class StashObjectPreview {
    
    var stashObjectId: String
    var title: String
    var latitude: Double
    var longitude: Double
    var distance: Double
    var stashScore: Int
    var lastUpdatedTimestamp: Int
    var expirationTimestamp: Int
    
    init(stashObjectPreviewFromJSON: StashObjectPreviewFromJSON) {
        self.stashObjectId = stashObjectPreviewFromJSON.stashObjectId
        self.title = stashObjectPreviewFromJSON.title
        self.latitude = stashObjectPreviewFromJSON.latitude
        self.lastUpdatedTimestamp = stashObjectPreviewFromJSON.lastUpdatedTimestamp
        self.longitude = stashObjectPreviewFromJSON.longitude
        self.expirationTimestamp = stashObjectPreviewFromJSON.expirationTimestamp
        self.distance = 0.0
        self.stashScore = stashObjectPreviewFromJSON.stashScore
        setDistance()
    }
    
    
    init(stashObjectId: String, title: String, latitude: Double, longitude: Double, stashScore: Int, lastUpdatedTimestamp: Int, expirationTimestamp: Int) {
        self.stashObjectId = stashObjectId
        self.title = title
        self.latitude = latitude
        self.lastUpdatedTimestamp = lastUpdatedTimestamp
        self.longitude = longitude
        self.expirationTimestamp = expirationTimestamp
        self.distance = 0.0
        self.stashScore = stashScore
        setDistance()
    }
    
    func setDistance() {
        guard let currentLocation = LocationProvider.shared.currentLocation else { return }
        let stashObjectLocation = CLLocation(latitude: self.latitude, longitude: self.longitude)
        self.distance = currentLocation.distance(from: stashObjectLocation)
    }
    
    func update(stashObjectPreview: StashObjectPreview) {
        if self.stashObjectId == stashObjectPreview.stashObjectId {
            self.title = stashObjectPreview.title
            self.latitude = stashObjectPreview.latitude
            self.lastUpdatedTimestamp = stashObjectPreview.lastUpdatedTimestamp
            self.longitude = stashObjectPreview.longitude
            self.expirationTimestamp = stashObjectPreview.expirationTimestamp
            self.stashScore = stashObjectPreview.stashScore
            setDistance()
        }
    }
}
