//
//  StashObjectSimplifiedFromJSON.swift
//  Stash
//
//  Created by Florian on 8/13/19.
//  Copyright © 2019 Florian Abel. All rights reserved.
//

import Foundation

struct StashObjectPreviewFromJSON: Decodable {
    var stashObjectId: String
    var title: String
    var latitude: Double
    var longitude: Double
    var stashScore: Int
    var lastUpdatedTimestamp: Int
    var expirationTimestamp: Int
}
