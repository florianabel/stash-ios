//
//  StashObjectRequest.swift
//  Stash
//
//  Created by Florian on 8/13/19.
//  Copyright © 2019 Florian Abel. All rights reserved.
//

import Foundation
import CoreLocation

class StashObjectRequest: Encodable{
    var stashObjectIds: [String]?
    var latitude: Double?
    var longitude: Double?
    var distanceFromLocation: Int?
    
    init() {
        
    }
    
    init(stashObjectIds: [String]){
        self.stashObjectIds = stashObjectIds
    }
    
    init(fromLocation location: CLLocation, withRadius radius: Int) {
        self.latitude = location.coordinate.latitude
        self.longitude = location.coordinate.longitude
        self.distanceFromLocation = radius
    }
}
