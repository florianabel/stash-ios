//
//  AuthenticationRequest.swift
//  Stash
//
//  Created by Florian on 8/24/19.
//  Copyright © 2019 Florian Abel. All rights reserved.
//

import Foundation

struct AuthenticationRequest: Encodable {
    var email: String
    var password: String
}
