//
//  RegisterResponse.swift
//  Stash
//
//  Created by Florian on 8/5/19.
//  Copyright © 2019 Florian Abel. All rights reserved.
//

import Foundation

struct AuthenticationResponse: Decodable {
    var userId: String
    var email: String?
    var token: String
}
