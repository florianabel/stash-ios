//
//  Session.swift
//  Stash
//
//  Created by Florian on 8/2/19.
//  Copyright © 2019 Florian Abel. All rights reserved.
//

import Foundation

class Session: Codable{
    private(set) var sessionId: String
    
    init(sessionId: String) {
        self.sessionId = sessionId
    }    
}
