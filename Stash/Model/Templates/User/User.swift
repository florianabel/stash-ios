//
//  File.swift
//  Stash
//
//  Created by Florian on 8/7/19.
//  Copyright © 2019 Florian Abel. All rights reserved.
//

import Foundation
import UIKit

class User {
    var userId: String?
    var email: String?
    var username: String?
    var token: String?
    var authString: String?
    var profileImage: UIImage?

    
    
    init(userId: String, email: String, token: String) {
        self.userId = userId
        self.email = email
        self.token = token
        self.authString = "Bearer " + token
    }
    
    func setUsername(username: String) {
        self.username = username
    }
    
    func setUserId(userId: String) {
        self.userId = userId
    }
    
    func setToken(token: String) {
        self.token = token
        self.authString = "Bearer " + token
    }
    
    func setProfileImage(image: UIImage) {
        self.profileImage = image
    }
    
    func setProfileImage(imageBase64: String) {
        self.profileImage = UIImage(data: Data(base64Encoded: imageBase64, options: .ignoreUnknownCharacters)!)
    }
}
