//
//  UserFromJSON.swift
//  Stash
//
//  Created by Florian on 8/7/19.
//  Copyright © 2019 Florian Abel. All rights reserved.
//

import Foundation

struct UserFromJSON: Decodable {
    var userId: String
    var email: String
    var token: String
    var username: String?
    var profileImage: String?
}
