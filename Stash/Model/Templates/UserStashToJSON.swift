//
//  File.swift
//  Stash
//
//  Created by Florian on 7/30/19.
//  Copyright © 2019 Florian Abel. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

class UserStashToJSON: Encodable{
    var tags: String
    var latitude: Double
    var longitude: Double
    var timestamp: Int
    var imageBase64: String?
    
    init(tags: String, location: CLLocation, image: UIImage?) {
        self.tags = tags
        self.latitude = location.coordinate.latitude
        self.longitude = location.coordinate.longitude
        self.timestamp = Int(Date().timeIntervalSince1970)
        if let image = image {
            let imageData = image.jpegData(compressionQuality: 0.4)
            self.imageBase64 = (imageData?.base64EncodedString(options: .lineLength64Characters))!
        }
    }
}
