//
//  stashPictureInteraction.swift
//  Stash
//
//  Created by Florian on 7/29/19.
//  Copyright © 2019 Florian Abel. All rights reserved.
//

import Foundation
import UIKit

protocol stashPictureInteraction: class {
    func setStashPicture(image: UIImage)
}
